from locust import HttpUser, task, between

class WebsiteUser(HttpUser):
    wait_time = between(1, 5)

    @task
    def load_home(self):
        self.client.get("/service1/")

    @task(3)
    def load_data(self):
        self.client.get("/service1/data")
