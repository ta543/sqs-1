import unittest
import requests

class TestService1Integration(unittest.TestCase):
    BASE_URL = "http://localhost:8000/service1"

    def test_home(self):
        response = requests.get(f"{self.BASE_URL}/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {"message": "Hello from Service 1!"})

    def test_data(self):
        response = requests.get(f"{self.BASE_URL}/data")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {"data": [1, 2, 3, 4, 5]})

if __name__ == '__main__':
    unittest.main()
