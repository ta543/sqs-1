from flask import Flask, jsonify

app = Flask(__name__)

@app.route('/')
def home():
    return jsonify({"message": "Welcome to Service 2"})

@app.route('/info')
def info():
    return jsonify({"info": "This is service 2 providing different information"})

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5001)
